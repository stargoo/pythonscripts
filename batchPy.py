#!/usr/bin/python

import argparse
import os
import shutil
import subprocess
import sys

myUserFolder     = "/Users/scott/Desktop/"
mySSTemplate     = "/Users/scott/Desktop/ss_template.bat"
myEXETemplate    = "/Users/scott/Desktop/exe_template.bat"
myMasterCopyFile = "/Users/scott/Desktop/master.bat"
copyFileFolder   = "/Users/scott/Desktop/copyfiles/"
sublimeApp       = "subl"  #sublime

def createPath(path):
    if not os.path.isdir(path):
        os.mkdir(path)

def checkFolder(answer):
    if answer == "y":
        print "Thanks!"
        createFolder()
    elif answer == "n":
        print "Please try again.  Exiting"
    else:
        print "Must answer with y or n.  Exiting"

def createFolder():
    print "Creating new Folder..."
    newPath = myUserFolder + scriptArgs.folder
    createPath(newPath)
    copyMasterCopyFile()

def copyMasterCopyFile():
    print "Creating new copyfile..."
    newName = copyFileFolder + scriptArgs.folder + ".bat"
    shutil.copyfile(myMasterCopyFile, newName)

def checkScript(scripttype):
    if scripttype == "ss":
        print "copying run batch file for ss"
        newName = myUserFolder + scriptArgs.folder + "/" + scriptArgs.folder + "_run.bat"
        shutil.copyfile(mySSTemplate, newName)
        openFiles(newName)
    elif scripttype == "exe":
        print "copying run batch file for exe"
        newName = myUserFolder  + scriptArgs.folder + "/" + scriptArgs.folder + "_run.bat"
        shutil.copyfile(myEXETemplate, newName)
        openFiles(newName)
    else:
        print "you did not specify the test type correctly.  Exiting."

def openFiles(nameOfFiles):
    nameOfBatch = copyFileFolder + scriptArgs.folder + ".bat"
    subprocess.call([sublimeApp, nameOfBatch, nameOfFiles])

def success():
    print "*********************"
    print "******SUCCESS!!******"
    print "*********************"

def header():
    os.system("clear")
    print "***********************************************************"
    print "batch file to create folders and copyfile.bat for new str's"
    print "***********************************************************"



def main(argv):
    header()
    parser = argparse.ArgumentParser(prog='batchPy.py', usage='%(prog)s [STRnumber_TestName]')
    parser.add_argument('folder', action="store")

    global scriptArgs
    scriptArgs=parser.parse_args()

    print "The script will create folder " + scriptArgs.folder

    userAnswer = raw_input('OK? (y or n): ')
    checkFolder(userAnswer)

    testType = raw_input('Is this an ss script or an exe file? (ss or exe): ')
    checkScript(testType.strip())

    success()

if __name__ == "__main__":
    main(sys.argv)






