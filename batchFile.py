########################################################################
#Includes
########################################################################

import optparse, sys, os, subprocess
from collections import deque

########################################################################
#Parser Options
########################################################################
usageText = "batchFile.py [options] FWREVISION, e.g. batchFile.py -t SSD_DITS E315"
parser = optparse.OptionParser(usage= usageText)

parser.add_option("-t", action="store", type="string", dest="startTestName", help="STRING Start test from TESTNAME and continue on mutiple tests")
parser.add_option("-l", action="store", type="int", dest="logData", default=0, help="INT [0,1,2] Log Data options. default=[%default]")
parser.add_option("-s", action="store", type="string", dest="testName", help="STRING Run single test specified by TESTNAME")
parser.add_option("-c", action="store_false", dest="copyScripts", default=True, help="BOOLEAN Copy scripts from Network.  [default=%default]")
parser.add_option("-b", action="store", type="int", dest="blockSize", help="Specify block size for the test.")
parser.add_option("-e", action="store_true", dest="testList", default=False, help="BOOLEAN List tests in order of runtime.  [default=%default]" )

########################################################################
#Global Variables
########################################################################

local_script_path       = 'c:\\temp\\fw_test\\exec\\'
perforce_bin            = 'c:\\program files\\perforce\\p4.exe"
local_python_scripts    = local_script_path + 'pythonscripts\\'
p4_login_path           = local_python_scripts + 'pr_login.py'
python_bin              = 'c:\\python26\\python.exe'
kodiak_bin              = 'c:\\fw_temp\\'


########################################################################
#Methods 
########################################################################

def standardOpts():
    if (options.blockSize):
        standard_options = '-nk' + ' ' +  '-fmt_size=' + string(blockSize)
    else:
        standard_options = '-nk'

def networkSetup(self):
    '''make sure we are connecting to the correct network drive'''
    if 'SPK' in os.environ['PC_NUM']
        network_drive = 'L:\\'
    else:
        network_drive = 'S:\\'
    print 'Using Network Drive ' + network_drive
    if copyScripts == 0:
        xcopyBin = 0
    else:
        xcopyBin = 'xcopy /I /Y /Z /E /Q' 
        if os.path.exists(perforce_bin):
            xcopy_path='c:\\temp\\'
            subprocess.call(perforce_bin, 'set P4CLIENT=SIE_SHK_'+os.environ['PC_NUM'], shell=True)
        else:
            xcopy_path=network_drive       

def perforceCheck(self):
    '''make sure we are logged into perforce'''
    if os.path.exists(perforce_bin):
            if os.path.exists(p4_login_path):
                subprocess.call(python_bin, p4_login_path, perforce_bin, shell=True)
            else:
                p4_User= raw_input("Perforce UserName: ")
                print perforce_bin + ' ' + '-u' + ' ' + p4_User ' login'
                subprocess.call(perforce_bin, '-u', p4_User, 'login', shell=True)
    else:
        print 'Perforce .exe file does not exist. Exiting...'
        exit(-1)

class newTest(object):
    '''base test class holds attributes for running each test'''
    def __init__(self, test_name, test_type, test_description, command_line_arguments, next_test):
        self.test_name=test_name
        if (test_name=='CHKRUN'):
            self.test_executable=sys.environ['DO_CHKRUN']
        else:
            self.test_executable=kodiak_bin + ' ' + test_name + '.cyc'
        self.test_type=test_type
        self.test_description=test_description
        self.command_line_arguments=command_line_arguments
        self.next_test=next_test
        self.syncFolder=syncFolder



bifi = newTest('BIFI','DVT', 'But_It_Finds_Issues', standard_options, tests_done, 'BIFI')
baseline = newTest('Baseline', 'DVT', 'BaselineSSD', standard_options, tests_done, 'BaselineSSD')
qt = newTest('QT', 'DVT', 'Quick_Test', standard_options + '-#loops=10 -#long=1', tests_done, 'Quick_Test')
tests_done = newTest('CHKRUN','NONE', 'Results Checker','-inf=' + sys.environ['CHKRUN_LIST'], '' )


# this dictionary maps the testname given in the argument to the newTest object name
testDictionary = {
                    'BIFI':bifi
                    'BASELINE':baseline
                    'QT':qt
                 }
testList = deque([])


if testName!='None':
    testList.append(testDictionary[testName.upper()])
if startTestName!='None':
    testList.append=(testDictionary[startTestname.upper()])
else: 
    print 'Please indicate either a multiple (-t) test or a single (-s) test\n'
    print usageText

while testList.next_test!=tests_done

    

if (options.testList):
    for test in tests:
        print test.test_name

def main():
    # parse command line options
    (options, args) = parser.parse_args()
    if len(args)!=1:
        print "Firmware Revision is a mandatory argument\n"
        print usageText
        exit(-1)
    else:
        fwString = sys.argv[-1]
        print "The Firmware Rev is " + fwString


    #setup Perforce
    perforceCheck()
    #setup Network
    networkSetup()
    #setup standard options
    standardOpts()




    if exist %PERFORCE_BIN% (
        %PERFORCE_BIN% sync //sie/tco/branches/ssd/FW_TEST/chkpt2/baselineSSD/script/*
    )         
# if testName!=None:
#     tests = (BIFI, WHATEVER, WHATEVER_2, WHATEVER_3)
# else tests=(testName)

# for test in tests:
#     test_execute_log(test)
