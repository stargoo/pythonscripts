#!/usr/bin/python

import os
import subprocess

import logging
import datetime
from time import sleep


SRC_DIR='/volume3/Media/Incoming/try this/'
DST_DIR="/volume3/Media/movies/"
MOD_FILE="/volume3/Media/Incoming/TV/mod_date.txt"


LOG_FILENAME = '/volume3/Media/mp4ConversionLog.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO,)


os.chdir(SRC_DIR)

#list the files
files = os.listdir(os.curdir)
for file in files:
    # check if it's an mkv file
    if '.mkv' in file:
        # set the new name
        newfilename = file.replace('.mkv', '.mp4')
        newfilefolderName = DST_DIR + newfilename
        # convert and check for return code
        try:
            subprocess.check_call(['ffmpeg', '-i', file, '-vcodec copy', '-acodec libfaac', '-ab 256k', 
                newfilefolderName])
            logging.info(str(datetime.datetime.now()) + "The conversion of " + file + " was successful")
        except subprocess.CalledProcessError:
            print "The conversion of " + file + "failed"
