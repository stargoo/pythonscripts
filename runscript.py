import os

SS = true
modified = true
testDirectory = '224352_FmtCertify'
testName = 'FmtCertify.exe'
testLogName = 'Gibson_OEM.log'
serverTestLogName = 'Server_Gibson_OEM.log'

baseDirectory = 'm:\\users\\scottg\\'
stx = 'c:\\users\\fte884\\desktop\\stx\\stx'
serverSS = 's:\\ssd\\fw_test\\exec\\stx\\tests\\'
serverEXE = 's:\\ssd\\fw_test\\exec\\opensfl\\'


# set the environment variable first
os.putenv('stx_script_path', 'c:\stx\;c:\stx\lib' )

# change directory to the correct user
os.chdir('c:\\users\\fte884')


modifiedTest = baseDirectory + testDirectory + '\\' + testName
modifiedTestLog = baseDirectory + testDirectory + '\\' + testLogName

# now we can run the modified test
subprocess.call([stx, modifiedTest, modifiedTestLog, '-r=2'])

# open up the log file
subprocess.call(['sublime_text', modifiedTestLog])

# if we want to run the server version and take a look:
if (modified):
    if (SS):
        serverTest = serverSS + testName
    else:
        serverTest = serverEXE + testName

    serverTestLog = baseDirectory + testDirectory + '\\' + serverTestLogName

    # the server version of the test can be run here
    subprocess.call([stx, serverTest, serverTestLog])

    # open up the log file
    subprocess.call(['sublime_text', serverTestLog])



