#!/usr/bin/python

import argparse
import os
import shutil
import subprocess
import sys
import string

def main(argv):

    parser = argparse.ArgumentParser(prog='batchTemplate.py', usage='%(prog)s [STRnumber_TestName]')
    parser.add_argument('folder', action="store")

    global scriptArgs
    scriptArgs=parser.parse_args()

    print "The script will create folder " + scriptArgs.folder

    userAnswer = raw_input('OK? (y or n): ')

    testType = raw_input('Is this an ss script or an exe file? (ss or exe): ')
    #testType.strip()

    orig = open("/Users/scott/Desktop/runscript_temp.py", "r")
    data = orig.read()
    orig.close()

    data_template=string.Template(data)

    new = open("/Users/scott/Desktop/runscript_filled.py", "w")
    print >> new, data_template.safe_substitute(name=scriptArgs.folder, customer='IBM', room='512', manager='Andy Joe')
 
    new.close()
 
if __name__ == "__main__":
    main(sys.argv)
