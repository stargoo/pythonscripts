#!/usr/bin/python

import logging
import datetime
from time import sleep


LOG_FILENAME = '/Users/scott/Desktop/myTestLog_1.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO,)


logging.info('here\'s the time: ' + str(datetime.datetime.now()))

sleep(4)

logging.info('4 seconds later: ' + str(datetime.datetime.now()))
f=open(LOG_FILENAME, 'rt')

try:
    body = f.read()
finally:
    f.close()

print 'FILE:'
print body

