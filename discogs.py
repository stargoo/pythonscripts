#!/usr/local/bin/python
import discogs_client as discogs
import urllib

#use a user agent so if discogs.com has an issue, they can contact me
discogs.user_agent = 'stargooDiscogsClient/0.1 +http://stargoo.dyndns.org'

#will have to parse an argument here for the artist 
#need to look up how to read an mp3 file and find out 
#it's metadata attributes for artist and album
mySearch = discogs.Artist('Boards of Canada')

foundAlbum = 0
foundImage = 0
#we have the artist, now list the releases
for rel in mySearch.releases:
    if foundAlbum==0:
        #search for the title - might want to pass an argument here
        if rel.data['title'] == 'Tomorrow\'s Harvest':
            foundAlbum=1
            output = rel.data['images']
            for image in output:
                if foundImage == 0:
                #look for a 600 px image
                    if image['height']==600:
                        print "found it!"
                        foundImage = 1

                        cover=urllib.URLopener()
                        cover.retrieve(image['uri'], "/Volumes/music/Boards of Canada - Tomorrow's Harvest (2012) - V0/Cover.jpg")
                        print 'Done'


