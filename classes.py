#!/usr/bin/python

class myClass:
    def __init__(self):
        self.classNum = 0

    def hw(self):
        print 'hello world'

    def myNum(self, num):
        self.classNum=num

    def printmyNum(self):
        print self.classNum    


x=myClass()
x.myNum(1)

y=myClass()
y.myNum(2)

z=myClass()
z.myNum(3)

l=(z,y,x)
for all in l:
    all.printmyNum()